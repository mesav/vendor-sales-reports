/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.data;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.27.
 * Time: 6:18
 * To change this template use File | Settings | File Templates.
 */
@Preload
public interface SaleItem extends Entity {

  //Integer getVendorAccountId();
  //void setVendorAccountId(Integer vendorAccountId);

  VendorAccount getVendorAccount();
  void setVendorAccount(VendorAccount vendorAccount);

  String getExpertName();

  void setExpertName(String expertName);

  @Indexed
  String getInvoice();

  @Indexed
  void setInvoice(String invoice);

  @Indexed
  Date getDateOfSale();

  @Indexed
  void setDateOfSale(Date date);

  @Indexed
  String getLicenseId();

  void setLicenseId(String licenseId);

  String getPluginKey();
  void setPluginKey(String pluginKey);

  String getPluginName();
  void setPluginName(String pluginName);

  @Indexed
  String getOrganisation();

  void setOrganisation(String organisation);

  String getTechnicalContactName();

  void setTechnicalContactName(String technicalContactName);

  String getTechnicalContactEmail();

  void setTechnicalContactEmail(String technicalContactEmail);

  String getBillingContactName();

  void setBillingContactName(String billingContactName);

  String getBillingContactEmail();

  void setBillingContactEmail(String billingContactEmail);

  String getCountry();

  void setCountry(String country);

  String getLicenseSize();

  void setLicenseSize(String licenseSize);

  String getLicenseType();

  void setLicenseType(String licenseType);

  String getSalesType();

  void setSalesType(String salesType);

  Double getSalesPrice();

  void setSalesPrice(Double salesPrice);

  Double getVendorAmount();

  void setVendorAmount(Double vendorAmount);

  String getMaintenanceStart();

  void setMaintenanceStart(String maintenanceStart);

  String getMaintenanceEnd();

  void setMaintenanceEnd(String maintenanceEnd);

  String getHosting();
  void setHosting(String hosting);

  String getBillingPeriod();
  void setBillingPeriod(String billingPeriod);

  String getAddonLicenseId();
  void setAddonLicenseId(String addonLicenseId);

  String getHostLicenseId();
  void setHostLicenseId(String hostLicenseId);

  String getRegion();
  void setRegion(String region);

  Date getLastUpdated();
  void setLastUpdated(Date lastUpdated);

}
