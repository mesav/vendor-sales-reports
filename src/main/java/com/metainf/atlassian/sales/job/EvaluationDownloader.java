/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.job;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.metainf.atlassian.sales.data.EvaluationItemDao;
import com.metainf.atlassian.sales.data.VendorAccount;
import com.metainf.atlassian.sales.data.VendorAccountDao;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by Hegyi on 2015.06.28..
 */
public class EvaluationDownloader implements Job {
  private static transient Logger logger = Logger.getLogger(EvaluationDownloader.class);
  private final TransactionTemplate transactionTemplate;
  private final EvaluationItemDao evaluationItemDao;
  private final VendorAccountDao vendorAccountDao;

  public EvaluationDownloader(TransactionTemplate transactionTemplate, EvaluationItemDao evaluationItemDao, VendorAccountDao vendorAccountDao) {
    this.transactionTemplate = transactionTemplate;
    this.evaluationItemDao = evaluationItemDao;
    this.vendorAccountDao = vendorAccountDao;
  }


  @Override
  public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
    try {
      this.transactionTemplate.execute(new TransactionCallback<Object>() {
        @Override
        public Object doInTransaction() {
          VendorAccount[] accounts = EvaluationDownloader.this.vendorAccountDao.findAll();

          for (VendorAccount vendor : accounts) {
            try {

            } catch (Exception e) {
              logger.error(e.getMessage(), e);
            }
          }//for
          return null;
        }
      });
    } catch (Exception e) {
      this.logger.error(e.getMessage(), e);
    }
  }
}//
