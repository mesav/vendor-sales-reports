/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales;

import java.text.MessageFormat;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.google.common.annotations.VisibleForTesting;

public enum Period {
	ALL("ALL"), THIS_WEEK("THIS WEEK"), THIS_MONTH("THIS MONTH"), THIS_YEAR("THIS YEAR"), LAST_WEEK(
			"LAST WEEK"), LAST_MONTH("LAST MONTH"), LAST_YEAR("LAST YEAR");

	String displayName;

	Period(String displayName) {
		this.displayName = displayName;
	}

	public static Period fromParam(String displayName) {
		Period[] values = Period.values();
		for (Period period : values) {
			if (period.displayName.toUpperCase().equals(displayName.toUpperCase())) {
				return period;
			}
		}
		throw new IllegalArgumentException(
				MessageFormat.format("No Period is associated with displayName {0}", displayName));
	}

	public Interval getInterval() {
		return getInterval(new DateTime());
	}

	@VisibleForTesting
	Interval getInterval(DateTime referenceDate) {
		// Sales API returns just dates, so we don't care about hours, seconds
		// and minutes, but just in case
		DateTime nowStart = referenceDate.minuteOfDay().withMinimumValue().millisOfDay().withMinimumValue();
		DateTime nowEnd = referenceDate.minuteOfDay().withMaximumValue().millisOfDay().withMaximumValue();
		Interval all = new Interval(nowStart.withYear(2000), nowEnd);
		//
		Interval thisWeek = new Interval(nowStart.dayOfWeek().withMinimumValue(),
				nowEnd.dayOfWeek().withMaximumValue());
		//
		Interval thisMonth = new Interval(nowStart.dayOfMonth().withMinimumValue(),
				nowEnd.dayOfMonth().withMaximumValue());
		//
		Interval thisYear = new Interval(nowStart.monthOfYear().withMinimumValue().dayOfMonth().withMinimumValue(),
				nowEnd.monthOfYear().withMaximumValue().dayOfMonth().withMaximumValue());
		switch (this) {
		case ALL:
			return all;
		case THIS_WEEK:
			return thisWeek;
		case THIS_MONTH:
			return thisMonth;
		case THIS_YEAR:
			return thisYear;
		case LAST_YEAR:
			return new Interval(thisYear.getStart().minusYears(1), thisYear.getEnd().minusYears(1));
		case LAST_MONTH: {
			DateTime start = thisMonth.getStart().minusMonths(1).dayOfMonth().withMinimumValue();
			DateTime end = nowEnd.minusMonths(1).dayOfMonth().withMaximumValue();
			return new Interval(start, end);
			}
		case LAST_WEEK:
			return new Interval(thisWeek.getStart().minusWeeks(1), thisWeek.getEnd().minusWeeks(1));
		default:
			throw new IllegalArgumentException(MessageFormat.format("Invalid period provided '{0}'", this));
		}
	}
}
