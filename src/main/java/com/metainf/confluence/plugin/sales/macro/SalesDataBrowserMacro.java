/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.confluence.plugin.sales.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.Group;
import com.atlassian.user.search.page.Pager;
import com.metainf.atlassian.sales.data.SaleItem;
import com.metainf.atlassian.sales.data.SaleItemDao;
import com.metainf.atlassian.sales.data.VendorAccount;
import com.metainf.atlassian.sales.data.VendorAccountDao;
import org.apache.commons.lang.StringUtils;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;

import static com.metainf.atlassian.sales.Params.PARAM_OUTPUTTYPE;
import static com.metainf.atlassian.sales.Params.PARAM_SORTDIRECTION;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.22.
 * Time: 15:13
 * To change this template use File | Settings | File Templates.
 */
public class SalesDataBrowserMacro extends SalesMacro implements com.atlassian.confluence.macro.Macro {
  public static final String OUTPUT_DETAILED = "Detailed";
  public static final String OUTPUT_AGGREGATEBYDAY = "Aggregate by day";
  public static final String OUTPUT_AGGREGATEBYWEEK = "Aggregate by week";
  public static final String OUTPUT_AGGREGATEBYMONTH = "Aggregate by month";
  public static final String OUTPUT_AGGREGATEBYYEAR = "Aggregate by year";
  public static final String OUTPUT_AGGREGATEBYADDON = "Aggregate by add-on";
  public static final String OUTPUT_AGGREGATEBYSALETYPE = "Aggregate by sale type";
  public static final String OUTPUT_AGGREGATEBYLICENSETYPE = "Aggregate by license type";
  public static final String OUTPUT_AGGREGATEBYLICENSESIZE = "Aggregate by license size";
  public static final String OUTPUT_AGGREGATEBYCOUNTRY = "Aggregate by country of sale";

  public static final String OUTPUT_TOTAL = "Totals only";

  // We just have to define the variables and the setters, then Spring injects the correct objects for us to use. Simple and efficient.
  // You just need to know *what* you want to inject and use.

  public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

  public SalesDataBrowserMacro(UserAccessor userAccessor, SaleItemDao saleItemDao, VendorAccountDao vendorAccountDao) {
    super(userAccessor, saleItemDao, vendorAccountDao);
  }

  //@Override
  public boolean hasBody() {
    return false;
  }

  //@Override
  public RenderMode getBodyRenderMode() {
    return null;
  }

  //@Override
  public String execute(Map<String,String> parameters, String body, RenderContext renderContext) throws MacroException {
    return view(parameters);
  }

  public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
    return view(parameters);
  }//

  @Override
  public BodyType getBodyType() {
    return BodyType.NONE;  //To change body of implemented methods use File | Settings | File Templates.
  }

  @Override
  public OutputType getOutputType() {
    return OutputType.BLOCK;
  }

  private String view(Map<String, String> parameters) {
    Map<String, Object> context = MacroUtils.defaultVelocityContext();
    boolean authorizedToViewDataWithPricing = isAuthorizedToViewDataIncludingPricing(AuthenticatedUserThreadLocal.get());
    boolean authorizedToViewDataWithoutPricing = isAuthorizedToViewDataWithoutPricing(AuthenticatedUserThreadLocal.get());

    context.put("authorizedToViewDataWithPricing", Boolean.valueOf(authorizedToViewDataWithPricing));
    context.put("authorizedToViewDataWithoutPricing", Boolean.valueOf(authorizedToViewDataWithoutPricing));

    if (authorizedToViewDataWithPricing || authorizedToViewDataWithoutPricing) {
    }

    //context.put("labels", labels);
    return VelocityUtils.getRenderedTemplate("com/metainf/confluence/plugin/sales/templates/data-browser.vm", context);
  }

}//
