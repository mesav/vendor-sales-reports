package com.metainf.confluence.plugin.sales.rest;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.metainf.atlassian.sales.ReportGenerator;
import com.metainf.atlassian.sales.data.SaleItem;
import com.metainf.atlassian.sales.data.SaleItemDao;
import com.metainf.atlassian.sales.data.VendorAccount;
import com.metainf.atlassian.sales.data.VendorAccountDao;
import org.apache.commons.lang.StringUtils;
import com.atlassian.user.Group;
import com.atlassian.user.search.page.Pager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import org.apache.log4j.Logger;

@Path("/data")
public class DataBrowserResource extends AbstractRESTResource {
	protected final UserAccessor userAccessor;
	protected final SaleItemDao saleItemDao;
	protected final VendorAccountDao vendorAccountDao;
	private static final Logger logger = Logger.getLogger(DataBrowserResource.class);

	public DataBrowserResource(UserAccessor userAccessor, SaleItemDao saleItemDao, VendorAccountDao vendorAccountDao) {
		this.userAccessor = userAccessor;
		this.saleItemDao = saleItemDao;
		this.vendorAccountDao = vendorAccountDao;
	}

	@GET
	public Response search(@QueryParam("term") String term) {
		try {
			ConfluenceUser user = AuthenticatedUserThreadLocal.get();
			if (user == null) {
				throw new Exception("Not allowed");
			}

			boolean authorizedToViewPricing = isAuthorizedToViewDataIncludingPricing(user);

			List<SaleItemModel> models = new ArrayList<>();

			if (StringUtils.isNotBlank(term)) {
				SaleItem[] sales = this.saleItemDao.filter("LICENSE_ID = ? OR INVOICE = ? OR ORGANISATION LIKE ?", term, term, "%"+term+"%");
				for (SaleItem si : sales) {
					models.add(new SaleItemModel(si, authorizedToViewPricing));
				}
			}
			return ok(models);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return error(e);
		}
	}

	protected boolean isAuthorizedToViewDataIncludingPricing(ConfluenceUser user) {
		return isMemberOfGroup(user, vendorAccount -> vendorAccount.getNotificationGroup());
	}

	protected boolean isAuthorizedToViewDataWithoutPricing(ConfluenceUser user) {
		return isMemberOfGroup(user, vendorAccount -> vendorAccount.getCustomerViewGroup());
	}

	protected boolean isMemberOfGroup(ConfluenceUser user, Function<VendorAccount, String> groupProvider) {
		boolean authorized = false;
		for (VendorAccount vendor : this.vendorAccountDao.findAll()) {
			String groupName = groupProvider.apply(vendor);
			Group group = groupName != null ? this.userAccessor.getGroup(groupName) : null;
			if (group != null) {
				Pager<String> members = this.userAccessor.getMemberNames(group);
				for (Iterator<String> it = members.iterator(); it.hasNext() && !authorized;) {
					String member = it.next();
					if (member != null && (member.equals(user.getKey()) || member.equals(user.getName()))) {
						authorized = true;
						break;
					}
				}
			}

		}
		return authorized;

	}
}
