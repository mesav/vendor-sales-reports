package com.metainf.confluence.plugin.sales.action;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.AlgorithmParameters;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import static java.util.Base64.Decoder;
import static java.util.Base64.Encoder;

/**
 * Created by Administrator on 9/1/2016.
 */
public class EncryptUtil
{
    private static SecureRandom random;

    private static char[] encryptionPassword;
    private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final String SECRET_KEY_ALGORITHM = "PBKDF2WithHmacSHA512";

    private static final int KEY_LENGTH = 128;
    private static final int ITERATIONS = 1000;

    static {
        EncryptUtil.initialize("owuezrqüweöőodmÍ:A?M,xcÉÍÍSlsédŐÚÖEWUTŐIWEH");
    }

    public static synchronized void initialize(String password)
    {
        try {
            random = SecureRandom.getInstance("SHA1PRNG");
            encryptionPassword = ("RSA/ECB/OAEPWithSHA-256AndMGF1Padding" + password).toCharArray();
        }
        catch (Exception e) {
            throw new SecurityException("Failed to initialize EncryptUtil!", e);
        }
    }

    public static String encrypt(String data)
    {
        if (data == null) {
            return null;
        }

        try {
            byte[] salt = new byte[16];
            random.nextBytes(salt);

            SecretKey secretKey = generateSecretKey(salt);

            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            AlgorithmParameters params = cipher.getParameters();
            byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] encryptedData = cipher.doFinal(data.getBytes("UTF-8"));

            Encoder base64 = Base64.getEncoder();
            StringBuffer sb = new StringBuffer(64);
            sb.append(base64.encodeToString(salt));
            sb.append('.');
            sb.append(base64.encodeToString(iv));
            sb.append('.');
            sb.append(base64.encodeToString(encryptedData));
            return sb.toString();
        }
        catch (Exception e) {
            throw new SecurityException(e);
        }
    }

    public static String decrypt(String data)
    {
        if (data == null) {
            return null;
        }

        try {
            String[] parts = data.split("\\.");
            if (parts == null || parts.length != 3) {
                return null;
            }

            Decoder b64 = Base64.getDecoder();
            byte[] salt = b64.decode(parts[0]);
            byte[] iv = b64.decode(parts[1]);
            byte[] encryptedData = b64.decode(parts[2]);

            SecretKey secretKey = generateSecretKey(salt);

            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            byte[] decryptedData = cipher.doFinal(encryptedData);

            return new String(decryptedData, "UTF-8");

        }
        catch (Exception e) {
            throw new SecurityException(e);
        }
    }

    private static SecretKey generateSecretKey(byte[] salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        SecretKeyFactory factory = SecretKeyFactory.getInstance(SECRET_KEY_ALGORITHM);
        PBEKeySpec spec = new PBEKeySpec(encryptionPassword, salt, ITERATIONS, KEY_LENGTH);
        SecretKey key = factory.generateSecret(spec);
        return new SecretKeySpec(key.getEncoded(), "AES");
    }
}
