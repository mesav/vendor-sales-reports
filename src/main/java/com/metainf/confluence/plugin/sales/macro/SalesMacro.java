/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.confluence.plugin.sales.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.Group;
import com.atlassian.user.search.page.Pager;
import com.metainf.atlassian.sales.data.SaleItem;
import com.metainf.atlassian.sales.data.SaleItemDao;
import com.metainf.atlassian.sales.data.VendorAccount;
import com.metainf.atlassian.sales.data.VendorAccountDao;
import com.metainf.confluence.plugin.sales.rest.SaleItemModel;
import org.apache.commons.lang.StringUtils;
import static com.metainf.atlassian.sales.Params.*;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.22.
 * Time: 15:13
 * To change this template use File | Settings | File Templates.
 */
public class SalesMacro /*extends BaseMacro*/ implements com.atlassian.confluence.macro.Macro {
  public static final String OUTPUT_DETAILED = "Detailed";
  public static final String OUTPUT_AGGREGATEBYDAY = "Aggregate by day";
  public static final String OUTPUT_AGGREGATEBYWEEK = "Aggregate by week";
  public static final String OUTPUT_AGGREGATEBYMONTH = "Aggregate by month";
  public static final String OUTPUT_AGGREGATEBYYEAR = "Aggregate by year";
  public static final String OUTPUT_AGGREGATEBYADDON = "Aggregate by add-on";
  public static final String OUTPUT_AGGREGATEBYSALETYPE = "Aggregate by sale type";
  public static final String OUTPUT_AGGREGATEBYLICENSETYPE = "Aggregate by license type";
  public static final String OUTPUT_AGGREGATEBYLICENSESIZE = "Aggregate by license size";
  public static final String OUTPUT_AGGREGATEBYCOUNTRY = "Aggregate by country of sale";
  public static final String OUTPUT_AGGREGATEBYEXPERT = "Aggregate by Expert";
  public static final String OUTPUT_AGGREGATEBYHOSTING = "Aggregate by Hosting";

  public static final String OUTPUT_TOTAL = "Totals only";

  // We just have to define the variables and the setters, then Spring injects the correct objects for us to use. Simple and efficient.
  // You just need to know *what* you want to inject and use.

  protected final UserAccessor userAccessor;
  protected final SaleItemDao saleItemDao;
  protected final VendorAccountDao vendorAccountDao;
  private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

  public SalesMacro(UserAccessor userAccessor, SaleItemDao saleItemDao, VendorAccountDao vendorAccountDao) {
    this.userAccessor = userAccessor;
    this.saleItemDao = saleItemDao;
    this.vendorAccountDao = vendorAccountDao;
  }

  //@Override
  public boolean hasBody() {
    return false;
  }

  //@Override
  public RenderMode getBodyRenderMode() {
    return null;
  }

  //@Override
  public String execute(Map<String,String> parameters, String body, RenderContext renderContext) throws MacroException {
    String outputType = (String)(parameters.containsKey(PARAM_OUTPUTTYPE) ? parameters.get(PARAM_OUTPUTTYPE) : null);

    if (StringUtils.isBlank(outputType)) {
      return detailedOutput(parameters);
    } else if ("Detailed".equalsIgnoreCase(outputType)) {
      return detailedOutput(parameters);
    } else {
      return aggregateOutput(parameters);
    }
  }

  public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
    String outputType = parameters.containsKey(PARAM_OUTPUTTYPE) ? parameters.get(PARAM_OUTPUTTYPE) : null;
    if (StringUtils.isBlank(outputType)) {
      return detailedOutput(parameters);
    } else if (OUTPUT_DETAILED.equalsIgnoreCase(outputType)) {
      return detailedOutput(parameters);
    } else {
      return aggregateOutput(parameters);
    }
  }//

  @Override
  public BodyType getBodyType() {
    return BodyType.NONE;  //To change body of implemented methods use File | Settings | File Templates.
  }

  @Override
  public OutputType getOutputType() {
    return OutputType.BLOCK;
  }

  @SuppressWarnings("unchecked")
private String aggregateOutput(Map<String, String> parameters) {
	
    @SuppressWarnings("rawtypes")
	Map context = MacroUtils.defaultVelocityContext();
    boolean authorizedToViewDataWithPricing = isAuthorizedToViewDataIncludingPricing(AuthenticatedUserThreadLocal.get());
    boolean authorizedToViewDataWithoutPricing = isAuthorizedToViewDataWithoutPricing(AuthenticatedUserThreadLocal.get());

    context.put("authorizedToViewDataWithPricing", Boolean.valueOf(authorizedToViewDataWithPricing));
    context.put("authorizedToViewDataWithoutPricing", Boolean.valueOf(authorizedToViewDataWithoutPricing));

    if (authorizedToViewDataWithPricing || authorizedToViewDataWithoutPricing) {
      SaleItem[] hits = this.saleItemDao.doSearch(parameters);
      String aggregateType = parameters.get(PARAM_OUTPUTTYPE);
      SalesAggregator aggregator = new SalesAggregator(SalesAggregator.AggregateType.getByTypeValue(aggregateType));

      List<SalesAggregator.AggregatedItem> aggregatedItems = aggregator.aggregate(hits);
      if("DESC".equals(parameters.get(PARAM_SORTDIRECTION))) {
    	  Collections.sort(aggregatedItems);
    	  Collections.reverse(aggregatedItems);
      } else {
    	  Collections.sort(aggregatedItems);
      }
      SalesAggregator.AggregatedItem total = aggregator.aggregateTotal(aggregatedItems);

      NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
      nf.setMaximumFractionDigits(2);
      NumberFormat pf = NumberFormat.getPercentInstance(Locale.US);
      pf.setMaximumFractionDigits(2);
      context.putAll(parameters);
      context.put("nf", nf);
      context.put("pf", pf);
      context.put("aggregateType", aggregateType);
      context.put("aggregatedItems", aggregatedItems);
      context.put("totalSalesPrice", authorizedToViewDataWithPricing ? total.getTotalSalesPrice() : null);
      context.put("totalVendorAmount", authorizedToViewDataWithPricing ? total.getTotalVendorAmount() : null);
      context.put("totalVendorNewSale", authorizedToViewDataWithPricing ? total.getTotalVendorNewSale() : null);
      context.put("totalVendorRenewal", authorizedToViewDataWithPricing ? total.getTotalVendorRenewal() : null);
      context.put("totalVendorUpgrade", authorizedToViewDataWithPricing ? total.getTotalVendorUpgrade() : null);

      context.put("newSaleShare", authorizedToViewDataWithPricing ? total.newSaleShare() : null);
      context.put("renewalShare", authorizedToViewDataWithPricing ? total.renewShare() : null);
      context.put("upgradeShare", authorizedToViewDataWithPricing ? total.upgradeShare() : null);

      context.put("dateOfFirstSale", total.getDateOfFirstSale());
      context.put("dateOfLastSale", total.getDateOfLastSale());
    }//
    //context.put("labels", labels);
    return VelocityUtils.getRenderedTemplate("com/metainf/confluence/plugin/sales/templates/aggregated-output.vm", context);
  } //

  private String detailedOutput(Map<String, String> parameters) {
    Map<String, Object> context = MacroUtils.defaultVelocityContext();
    boolean authorizedToViewDataWithPricing = isAuthorizedToViewDataIncludingPricing(AuthenticatedUserThreadLocal.get());
    boolean authorizedToViewDataWithoutPricing = isAuthorizedToViewDataWithoutPricing(AuthenticatedUserThreadLocal.get());

    context.put("authorizedToViewDataWithPricing", Boolean.valueOf(authorizedToViewDataWithPricing));
    context.put("authorizedToViewDataWithoutPricing", Boolean.valueOf(authorizedToViewDataWithoutPricing));

    if (authorizedToViewDataWithPricing || authorizedToViewDataWithoutPricing) {
      SaleItem[] hits = this.saleItemDao.doSearch(parameters);//this.saleItemDao.findAll("DATE_OF_SALE DESC");

      Date dateOfFirstSale = null;
      Date dateOfLastSale = null;

      ArrayList salesModels = new ArrayList();

      SalesAggregator.AggregatedItem total = new SalesAggregator.AggregatedItem(null);
      if (hits != null) {
        for (SaleItem si : hits) {
          total.addSale(si);

          if (dateOfFirstSale == null || dateOfFirstSale.compareTo(si.getDateOfSale()) > 0) {
            dateOfFirstSale = si.getDateOfSale();
          }

          if (dateOfLastSale == null || dateOfLastSale.compareTo(si.getDateOfSale()) < 0) {
            dateOfLastSale = si.getDateOfSale();
          }

          salesModels.add(new SaleItemModel(si, authorizedToViewDataWithPricing));
        }
      }//if

      NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
      nf.setMaximumFractionDigits(1);

      context.putAll(parameters);
      context.put("sales", salesModels);
      context.put("sdf", this.sdf);
      context.put("nf", nf);
      context.put("dateOfFirstSale", dateOfFirstSale == null ? "" : this.sdf.format(dateOfFirstSale));
      context.put("dateOfLastSale", dateOfLastSale == null ? "" : this.sdf.format(dateOfLastSale));

      context.put("totalSalesPrice", authorizedToViewDataWithPricing ? total.getTotalSalesPrice() : null);
      context.put("totalVendorAmount", authorizedToViewDataWithPricing ? total.getTotalVendorAmount() : null);
      context.put("totalVendorNewSale", authorizedToViewDataWithPricing ? total.getTotalVendorNewSale() : null);
      context.put("totalVendorRenewal", authorizedToViewDataWithPricing ? total.getTotalVendorRenewal() : null);
      context.put("totalVendorUpgrade", authorizedToViewDataWithPricing ? total.getTotalVendorUpgrade() : null);

      context.put("newSaleShare", authorizedToViewDataWithPricing ? total.newSaleShare() : null);
      context.put("renewalShare", authorizedToViewDataWithPricing ? total.renewShare() : null);
      context.put("upgradeShare", authorizedToViewDataWithPricing ? total.upgradeShare() : null);
    }
    //context.put("labels", labels);
    return VelocityUtils.getRenderedTemplate("com/metainf/confluence/plugin/sales/templates/detailed-output.vm", context);
  }

  protected boolean isAuthorizedToViewDataIncludingPricing(ConfluenceUser user) {
    return isMemberOfGroup(user, vendorAccount -> vendorAccount.getNotificationGroup());
  }

  protected boolean isAuthorizedToViewDataWithoutPricing(ConfluenceUser user) {
    return isMemberOfGroup(user, vendorAccount -> vendorAccount.getCustomerViewGroup());
  }

  protected boolean isMemberOfGroup(ConfluenceUser user, Function<VendorAccount, String> groupProvider) {
    boolean authorized = false;
    for (VendorAccount vendor : this.vendorAccountDao.findAll()) {
      String groupName = groupProvider.apply(vendor);
      Group group = groupName != null ? this.userAccessor.getGroup(groupName) : null;
      if (group != null) {
        Pager<String> members = this.userAccessor.getMemberNames(group);
        for (Iterator<String> it = members.iterator(); it.hasNext() && !authorized;) {
          String member = it.next();
          if (member != null && (member.equals(user.getKey()) || member.equals(user.getName()))) {
            authorized = true;
            break;
          }
        }
      }

    }
    return authorized;

  }

}//
