package com.metainf.confluence.plugin.sales.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by TiborHegyi on 2019. 03. 17..
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ErrorModel {
	private int errorCode;
	private List<String> errorMessages = new LinkedList<>();

	public ErrorModel() {
	}

	public ErrorModel(String errorMessage) {
		this.errorMessages.add(errorMessage);
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void addErrorMessage(String errorMessage) {
		this.errorMessages.add(errorMessage);
	}
}
