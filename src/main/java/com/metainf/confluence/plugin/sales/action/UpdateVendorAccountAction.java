/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.confluence.plugin.sales.action;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.user.GroupManager;
import com.metainf.atlassian.sales.data.VendorAccount;
import com.metainf.atlassian.sales.data.VendorAccountDao;
import org.apache.commons.lang.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.29.
 * Time: 14:30
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("serial")
public class UpdateVendorAccountAction extends ConfigurePluginAction {

  private Long accountId;
  private String notificationGroup;
  private String customerViewGroup;
  private String notificationType;
  private String offset;

  public UpdateVendorAccountAction(VendorAccountDao vendorAccountDao, ApplicationProperties applicationProperties, GroupManager groupManager) {
    super(vendorAccountDao, applicationProperties, groupManager);
  }

  @Override
  public String execute() throws Exception {
    if (this.accountId != null) {
      try {
        VendorAccount account = this.vendorAccountDao.getById(this.accountId);
        if (account != null) {
          account.setNotificationType(this.notificationType);
          account.setNotificationGroup(this.notificationGroup);
          account.setCustomerViewGroup(this.customerViewGroup);
          if (StringUtils.isNotBlank(this.offset) && StringUtils.isNumeric(this.offset)) {
            account.setLastOffset(Integer.valueOf(this.offset));
          }
          this.vendorAccountDao.update(account);

        }
      } catch (Exception e) {

      }
    }
    return super.execute();
  }

  public Long getAccountId() {
    return accountId;
  }

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  public String getNotificationGroup() {
    return notificationGroup;
  }

  public void setNotificationGroup(String notificationGroup) {
    this.notificationGroup = notificationGroup;
  }

  public String getNotificationType() {
    return notificationType;
  }

  public void setNotificationType(String notificationType) {
    this.notificationType = notificationType;
  }

  public String getCustomerViewGroup() {
    return customerViewGroup;
  }

  public void setCustomerViewGroup(String customerViewGroup) {
    this.customerViewGroup = customerViewGroup;
  }

  public String getOffset() {
    return offset;
  }

  public void setOffset(String offset) {
    this.offset = offset;
  }
}
//
